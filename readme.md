## Docker container for [duplicati](https://github.com/duplicati/duplicati) based on [bitnami/centos-extras-base](https://github.com/bitnami/centos-extras-base)

### Available on


[Quay.io](https://quay.io/repository/kristopherkram/duplicati)

[Docker Hub](https://hub.docker.com/repository/docker/kristopherkram/duplicati)

[Gitlab](https://gitlab.com/kristopherkram/duplicati/container_registry)


`docker pull quay.io/kristopherkram/duplicati --webservice-port=8200 --webservice-interface=any --webservice-password=YOUR_STRONG_PASSWORD_HERE`

[Issues](https://gitlab.com/kristopherkram/duplicati/-/issues)

[Source code](https://gitlab.com/kristopherkram/duplicati)

![Docker Pulls](https://img.shields.io/docker/pulls/kristopherkram/duplicati)


[![](https://images.microbadger.com/badges/image/kristopherkram/duplicati.svg)](https://microbadger.com/images/kristopherkram/duplicati "Get your own image badge on microbadger.com")