FROM bitnami/centos-extras-base
RUN rpmkeys --import "http://pool.sks-keyservers.net/pks/lookup?op=get&search=0x3fa7e0328081bff6a14da29aa6a19b38d3d831ef"
RUN su -c 'curl https://download.mono-project.com/repo/centos7-stable.repo | tee /etc/yum.repos.d/mono-centos7-stable.repo'
RUN curl -L https://github.com/duplicati/duplicati/releases/download/v2.0.5.1-2.0.5.1_beta_2020-01-18/duplicati-2.0.5.1-2.0.5.1_beta_20200118.noarch.rpm -o /root/release.rpm
RUN yum update -y
RUN yum install mono-core desktop-file-utils libappindicator -y
RUN rpm -i --nodeps /root/release.rpm
EXPOSE 8200/tcp
ENTRYPOINT ["/usr/bin/duplicati-server"]